/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.midterm_no1;

/**
 *
 * @author focus
 */
public class TestFootball {

    public static void main(String[] args) {
        //สร้าง Object เพื่อหาพื้นที่ของลูกฟุตบอล ของลีโอและเพื่อน
        Football football1 = new Football("Leo", 11); //สร้าง Object
        football1.calArea();//เรียกใช้สมการจาก calArea ของ Class Football
        System.out.println("Football Of Leo is : ");
        System.out.println(football1.calArea() + " unit square");
        System.out.println(" ");
        Football football2 = new Football("Friend", 10); //สร้าง Object
        football2.calArea();//เรียกใช้สมการจาก calArea ของ Class Football
        System.out.println("Football Of Friend is : ");
        System.out.println(football2.calArea() + " unit square");
        System.out.println("  ");
    }
}
