/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.midterm_no1;

/**
 *
 * @author focus
 */
public class Football {
    // ตอนนี้ยังเป็นแค่ Class อยู่
    //กำหนดAttribut------

    private String name;
    private double r;
    public static final double pi = 22.0 / 7;
    // -----------------------------------------

    public Football(String name ,double r) {
        this.name = name;
        this.r = r;
    }

    public double calArea() {//กำหนดสมการที่ใช้หาค่าของลูกฟุตบอล
        return pi * (r*r);
        
    }

}
